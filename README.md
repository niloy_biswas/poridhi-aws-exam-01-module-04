## Poridhi AWS Exam-01 Module-04

#### Some context behind naming scheme followed in this exam
```
VPC-1 => niloy-01-vpc (CIDR - 10.20.0.0/16)
VPC-2 => niloy-02-vpc (CIDR - 10.21.0.0/16)
EC2-1 => niloy-01-bastion
EC2-2 => niloy-01-server-01
EC2-3 => niloy-02-server-01
Peering Connection => niloy-exam-pc
```

* Step-1: Create the first VPC with both public and private subnet, make sure to add NAT gateway to connect to the internet from private subnet. CIDR used for VPC-1 is `10.20.0.0/16`
<p align="center"> 
    <img src="./screenshots/mpv-shot0241.jpg" />
</p>

* Step-2: Create the second VPC with only private subnet, since we will be accessing the EC2 instance of first VPC's private subnet from this subnets EC2 instance, and this EC2 instance itself will not connect to the outside world. CIDR used here is `10.21.0.0/16`
<p align="center"> 
    <img src="./screenshots/mpv-shot0243.jpg" />
</p>

* Step-3: Wait for the VPC to be in available state. Since it may take ones containing internet gateways longer to finish.
<p align="center"> 
    <img src="./screenshots/mpv-shot0244.jpg" />
</p>

* Step-4: Create the first EC2 instance from the first VPC which is a bastion server. Edit the network settings and make sure that it is being created from the first VPC and if the subnet is in the public subnet of the first VPC. Also enable auto assign public IP for this one, since it is the entrypoint to our system
<p align="center"> 
    <img src="./screenshots/mpv-shot0246.jpg" />
</p>

* Step-5: Create another instance from the first VPC and edit the network settings to put it in the private subnet and also disable auto assign public IP
<p align="center"> 
    <img src="./screenshots/mpv-shot0247.jpg" />
</p>

* Step-6: Just like Step-5, create another EC2 instance, but this time from the second VPC and also put it in the private subnet
<p align="center"> 
    <img src="./screenshots/mpv-shot0248.jpg" />
</p>

* Step-7: Before continuing further, copy the ssh keys to each of the instances using `scp -i <path-to-key> <path-to-key> ubuntu@<instance-ip>:<destination-path>`
<p align="center"> 
    <img src="./screenshots/mpv-shot0250.jpg" />
</p>

* Step-8: Set hostname for each instances to better distinguish them using `sudo hostnamectl set-hostname <name-of-your-choice>`
<p align="center"> 
    <img src="./screenshots/mpv-shot0251.jpg" />
</p>

* Step-9: Create a `Peering Connection` from aws console. Simply search for peering connection and select the first available result and then put your desired name in `Name - optional` field. Click on `VPC ID (Requester)` and select the first vpc and `VPC ID (Accepter)` and select the second vpc. Then click on proceed or create.
<p align="center"> 
    <img src="./screenshots/mpv-shot0252.jpg" />
</p>

* Step-10: After that go to `Route Tables` of each private instances and click on `Edit routes`, then `Add route` and enter private subnet address of the VPC that we wish to connect. In this example `10.21.0.0/16` is the private subnet of second VPC and `10.20.0.0/16` is the private subnet of first VPC. Select `Peering Connection` for `Target` dropdown and select the peering connection we created in Step-9.  
 
***Route table of private EC2 instance of first VPC:***
<p align="center"> 
    <img src="./screenshots/mpv-shot0253.jpg" />
</p>

***Route table of private EC2 instance of second VPC:***
<p align="center"> 
    <img src="./screenshots/mpv-shot0254.jpg" />
</p>

* Step-11: Install nginx using `sudo apt install nginx` to the private EC2 instance of the first VPC. It should install and run a nginx server which runs on port 80.

* Step-12: Go to the security group settings of the private EC2 instance of the first VPC and add another setting where it allows HTTP and port 80 should automatically be set.

* Step-13: This is the final step. `SSH` into your bastion and then to the private EC2 instance of the second VPC and run `curl <ip-of-private-ec2-from-first-vpc>`, and it should return the raw index.html tags of nginx default screen in the terminal. There you have it, getting access to the nginx server from one vpc to another from private subnets.
<p align="center"> 
    <img src="./screenshots/mpv-shot0255.jpg" />
</p>